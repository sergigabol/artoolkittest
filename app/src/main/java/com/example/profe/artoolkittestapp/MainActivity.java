package com.example.profe.artoolkittestapp;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import org.artoolkit.ar.base.ARActivity;
import org.artoolkit.ar.base.assets.AssetHelper;
import org.artoolkit.ar.base.rendering.ARRenderer;

public class MainActivity extends ARActivity implements View.OnTouchListener {

    MyDefaultRenderer defaultRenderer;
    Button rotateLeftButton;
    Button moveButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AssetHelper assetHelper = new AssetHelper(getAssets());
        assetHelper.cacheAssetFolder(this, "Data");

        defaultRenderer=new JPCTRenderer(this);
        //defaultRenderer=new MyDefaultRenderer();
        rotateLeftButton=(Button)findViewById(R.id.buttonRotate);
        rotateLeftButton.setOnTouchListener(this);

        moveButton=(Button)findViewById(R.id.buttonMove);
        moveButton.setOnTouchListener(this);

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        switch(v.getId()){
            case R.id.buttonRotate:
                switch(event.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        //activem la rotacio
                        defaultRenderer.startRotationLeft();
                        break;
                    case MotionEvent.ACTION_UP:
                        //aturem la rotacio
                        defaultRenderer.stopRotationLeft();
                        break;
                }

                break;
            case R.id.buttonMove:
                switch(event.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        //activem la rotacio
                        defaultRenderer.startMove();
                        break;
                    case MotionEvent.ACTION_UP:
                        //aturem la rotacio
                        defaultRenderer.stopMove();
                        break;
                }

                break;
        }

        return true;
    }

    @Override
    protected ARRenderer supplyRenderer() {
        return defaultRenderer;
    }

    @Override
    protected FrameLayout supplyFrameLayout() {
        return (FrameLayout)findViewById(R.id.ARframe);
    }
}
