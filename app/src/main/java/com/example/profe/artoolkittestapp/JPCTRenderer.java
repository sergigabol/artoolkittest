package com.example.profe.artoolkittestapp;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.threed.jpct.Camera;
import com.threed.jpct.CollisionEvent;
import com.threed.jpct.CollisionListener;
import com.threed.jpct.FrameBuffer;
import com.threed.jpct.Light;
import com.threed.jpct.Loader;
import com.threed.jpct.Matrix;
import com.threed.jpct.Object3D;
import com.threed.jpct.Primitives;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;
import com.threed.jpct.World;
import com.threed.jpct.util.BitmapHelper;

import org.artoolkit.ar.base.ARToolKit;

import java.io.IOException;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by Profe on 16/12/2015.
 */
public class JPCTRenderer extends MyDefaultRenderer {

    World world;
    FrameBuffer frameBuffer;
    Camera camera;
    Object3D cube;
    Object3D tank;
    Object3D [] mines;
    Matrix [] minesMatrices;
    Matrix [] minesAuxMatrices;
    SimpleVector ellipsoid; //the ellipsoid size containing the tank

    Context context;

    boolean markerVisible=false;

    public JPCTRenderer(Context context){
        this.context=context;
    }

    @Override
    public boolean configureARScene() {
        configureJPCTWorld();
        return super.configureARScene();
    }

    public void configureJPCTWorld(){
        world=new World();
        world.setAmbientLight(20, 20, 20);

        Light sun=new Light(world);
        sun.setIntensity(250, 250, 250);

        camera=world.getCamera();

        Texture cubeText=
                new Texture(
                        BitmapHelper.rescale(
                                BitmapHelper.convert(
                                       context.
                                               getResources().
                                               getDrawable(R.drawable.texture)
                                ),128,128
                        )
                );

        TextureManager.getInstance().addTexture("cubeTexture",cubeText);

        Texture tankTexture=
                new Texture(
                        BitmapHelper.rescale(
                                BitmapHelper.convert(
                                        context.
                                                getResources().
                                                getDrawable(R.drawable.tanktexture)
                                ),128,128
                        )
                );
        TextureManager.getInstance().addTexture("tankTexture",tankTexture);

        cube = Primitives.getCube(40);
        cube.calcTextureWrapSpherical();
        cube.setTexture("cubeTexture");
        cube.strip();
        cube.build();

        try {
            AssetManager assetManager =
                    context.getAssets();
            Object3D[] objects =
                    Loader.load3DS(assetManager.open("Tank.3DS"), 0.1f);

            tank= Object3D.mergeAll(objects);
            tank.setTexture("tankTexture");
            float[] bounds=tank.getMesh().getBoundingBox();
            ellipsoid=new SimpleVector(
                    Math.abs(bounds[1]-bounds[0])/2,
                    Math.abs(bounds[3]-bounds[2])/2,
                    Math.abs(bounds[5]-bounds[4])/2
                );
            tank.strip();
            tank.build();

            tank.setCollisionMode(Object3D.COLLISION_CHECK_SELF);

            tank.addCollisionListener(new CollisionListener() {
                @Override
                public void collision(CollisionEvent collisionEvent) {
                    //restart movement

                    movementX=0;
                    movementY=0;
                    angle=0;

                }

                @Override
                public boolean requiresPolygonIDs() {
                    return false;
                }
            });


        }catch(IOException e){
            Log.e("RENDERER-JPCT","Error loading tank",e);
        }

        int numMines=(int)Math.floor(Math.random()*4+3);
        mines=new Object3D[numMines];
        minesMatrices=new Matrix[numMines];
        minesAuxMatrices=new Matrix[numMines];

        for(int i=0; i<numMines; i++){
            Object3D mine = Primitives.getCone(10);
            mine.calcTextureWrapSpherical();
            mine.setTexture("cubeTexture");
            mine.strip();
            mine.build();

            Matrix mineMatrix=new Matrix();
            Matrix mineAuxMatrix=new Matrix();

            float randomDist=(float)(110+31*Math.random());
            float randomAngle=(float)(360*Math.random());

            float randomX=randomDist*(float)Math.cos(Math.toRadians(randomAngle));
            float randomY=randomDist * (float) Math.sin(Math.toRadians(randomAngle));

            mineMatrix.rotateX((float)Math.toRadians(90));
            mineMatrix.translate(
                    randomX,
                    randomY,
                    0
            );

            Log.w("RENDERER","placing mine at "+randomX+","+randomY);

            minesMatrices[i]=mineMatrix;
            minesAuxMatrices[i]=mineAuxMatrix;
            mines[i]=mine;

            //set up collision events
            mine.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);


        }

        //MemoryHelper.compact();

    }

    @Override
    public void onSurfaceChanged(GL10 unused, int w, int h) {
        super.onSurfaceChanged(unused, w, h);
        if(frameBuffer!=null){
            frameBuffer.dispose();
        }
        frameBuffer=new FrameBuffer(unused,w,h);
    }



    //declare the matrix used on each frame here,
    // so they are reused and GC's are minimzed

    //the matrixes used. See
    // -----------------------------------------------
    // http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/
    // -----------------------------------------------
    Matrix projMatrix=new Matrix(); //the matrix having the camera projection
    Matrix markerMatrix=new Matrix(); //the matrix having the transformation to place the tank in the marker
    Matrix modelMatrix=new Matrix(); //the matrix having the model (tank) rotation and translation
    SimpleVector movementPrevision=new SimpleVector();
    SimpleVector correctedMovement;

    @Override
    public void draw(GL10 gl) {

        world.removeAllObjects();
        markerVisible=false;

        float[] projectionCamera=ARToolKit.getInstance().getProjectionMatrix();


        projMatrix.setIdentity();

        projMatrix.setDump(projectionCamera);
        projMatrix.transformToGL();
        SimpleVector translation=projMatrix.getTranslation();
        SimpleVector dir=projMatrix.getZAxis();
        SimpleVector up=projMatrix.getYAxis();
        camera.setPosition(translation);
        camera.setOrientation(dir, up);

        if(ARToolKit.getInstance().queryMarkerVisible(markerId1)){
            //Matrix markerMatrix=new Matrix();
            markerMatrix.setIdentity();
            //Log.e("RENDERER", "Marker1 detected");

            float[] marker1Transformation=
                    ARToolKit.
                            getInstance().
                            queryMarkerTransformation(markerId1);

            markerMatrix.setDump(marker1Transformation);
            markerMatrix.transformToGL();

            //cube.clearTranslation();
            //cube.clearRotation();

            cube.setRotationMatrix(markerMatrix);
            cube.setTranslationMatrix(markerMatrix);

            world.addObject(cube);

        }
        if(ARToolKit.getInstance().queryMarkerVisible(markerId2)){
            markerVisible=true;

            //Reset the matrix
            markerMatrix.setIdentity();
            modelMatrix.setIdentity();
            tank.clearTranslation();
            tank.clearRotation();

            //the position and rotation of the marker gives us the transformation matrix
            float[] markerTransformation=
                    ARToolKit.
                            getInstance().
                            queryMarkerTransformation(markerId2);

            markerMatrix.setDump(markerTransformation);
            markerMatrix.transformToGL();

            //display the mines

            for(int i=0; i<mines.length; i++){
                Object3D mine=mines[i];
                Matrix mineTrans=minesMatrices[i];
                Matrix mineAuxMatrix=minesAuxMatrices[i];

                mine.clearRotation();
                mine.clearTranslation();
                mineAuxMatrix.setIdentity();

                mineAuxMatrix.matMul(mineTrans);
                mineAuxMatrix.matMul(markerMatrix);

                mine.setRotationMatrix(mineAuxMatrix);
                mine.setTranslationMatrix(mineAuxMatrix);

                world.addObject(mine);

            }

            //do the model transformations (rotate first and translate then)
            //the angle must be negative, or x-y movement swapped, as JPCT has an inverted z-axis

            modelMatrix.rotateZ((float) Math.toRadians(-angle+180));
            modelMatrix.translate(movementX, movementY, 0);

            //now multiply trasnformationMat * modelMat

            modelMatrix.matMul(markerMatrix);

            //apply the resulting matrix to the tank.

            tank.setRotationMatrix(modelMatrix);
            tank.setTranslationMatrix(modelMatrix);

            world.addObject(tank);

            //check if the movement implies a collision
            movementPrevision.set(nextStepX, nextStepY, 0);
            movementPrevision.matMul(markerMatrix);
            correctedMovement=tank.checkForCollisionEllipsoid(movementPrevision,ellipsoid,2);

            if(!correctedMovement.equals(movementPrevision)){
                movementX=0;
                movementY=0;
            }

        }

        frameBuffer.clear();
        world.renderScene(frameBuffer);
        world.draw(frameBuffer);
        frameBuffer.display();

        if(markerVisible)updateMovement(4,2);

    }

    protected float nextStepX=0,nextStepY=0;

    protected void updateMovement(){
        updateMovement(1,1);
    }
    protected void updateMovement(float factorMovement, float factorAngle){
        if(rotateLeft){
            angle=(angle+factorAngle)%360;
        }
        if(move) {
            movementX+=nextStepX;
            movementY+=nextStepY;
            nextStepX=factorMovement*(float)Math.cos(Math.toRadians(angle));
            nextStepY=factorMovement*(float)Math.sin(Math.toRadians(angle));
        }
    }
}
