package com.example.profe.artoolkittestapp;

import android.util.Log;

import org.artoolkit.ar.base.ARToolKit;
import org.artoolkit.ar.base.rendering.ARRenderer;
import org.artoolkit.ar.base.rendering.Cube;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by Profe on 15/12/2015.
 */
public class MyDefaultRenderer extends ARRenderer {

    protected int markerId1=-1;
    protected int markerId2=-1;
    private Cube cube=new Cube(40,0,0,20);
    private Cube cube2=new Cube(20,0,0,10);

    protected float angle=0;
    protected boolean rotateLeft=false;

    protected float movementX=0;
    protected float movementY=0;
    protected boolean move=false;

    boolean markerDetected=false;

    @Override
    public boolean configureARScene() {

        markerId1=ARToolKit.getInstance().
                addMarker("single;Data/patt.kanji;80");
        markerId2=ARToolKit.getInstance().
                addMarker("single;Data/patt.hiro;80");

        Log.i("RENDERER", "Markers loaded: " + markerId1 + ", " + markerId2);

        if(markerId1<0)return false;

        return true;
    }

    @Override
    public void draw(GL10 gl) {

        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadMatrixf(ARToolKit.getInstance().getProjectionMatrix(), 0);

        gl.glEnable(GL10.GL_CULL_FACE);
        gl.glShadeModel(GL10.GL_SMOOTH);
        gl.glEnable(GL10.GL_DEPTH_TEST);
        gl.glFrontFace(GL10.GL_CW);

        markerDetected=false;

        if(ARToolKit.getInstance().queryMarkerVisible(markerId1)){
            markerDetected=true;
            Log.i("RENDERER", "marker1 detected");
            gl.glMatrixMode(GL10.GL_MODELVIEW);
            gl.glLoadMatrixf(ARToolKit.
                    getInstance().
                    queryMarkerTransformation(markerId1), 0);

            gl.glTranslatef(movementX,movementY,0);
            gl.glRotatef(angle,0,0,1);

            cube.draw(gl);


        }
        if(ARToolKit.getInstance().queryMarkerVisible(markerId2)){
            Log.i("RENDERER","marker2 detected");
            gl.glMatrixMode(GL10.GL_MODELVIEW);
            gl.glLoadMatrixf(ARToolKit.getInstance().queryMarkerTransformation(markerId2), 0);
            cube2.draw(gl);
            markerDetected=true;
        }

        if(markerDetected)updateMovement();



    }

    public void startRotationLeft(){
        rotateLeft=true;
    }
    public void stopRotationLeft(){
        rotateLeft=false;
    }

    public void startMove(){
        move=true;
    }
    public void stopMove(){
        move=false;
    }

    protected void updateMovement(){
        updateMovement(1,1);
    }
    protected void updateMovement(float factorMovement, float factorAngle){
        if(rotateLeft){
            angle=(angle+factorAngle)%360;
        }
        if(move) {
            movementX+=factorMovement*Math.cos(Math.toRadians(angle));
            movementY+=factorMovement*Math.sin(Math.toRadians(angle));
        }
    }

}
